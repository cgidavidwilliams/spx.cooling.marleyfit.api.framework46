﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;
using System;

namespace SPX.Cooling.MarleyFitAPI.Domain
{
    public class BasicInput : IBasicInput
    {
        public SelectionInputs Input { get; set; }
        public Account AccountInfo { get; set; }
    }
}
