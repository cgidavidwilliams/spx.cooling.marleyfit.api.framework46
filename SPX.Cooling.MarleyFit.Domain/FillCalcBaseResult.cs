﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;
using System;
using System.Collections.Generic;

namespace SPX.Cooling.MarleyFitAPI.Domain
{
    public class FillCalcBaseResult : IFillCalcBaseResult
    {
        private List<string> errorMessages = null;
        internal ErrorTypes? ErrorTypes { get; set; }

        public List<string> ErrorMessages
        {
            get
            {
                if (errorMessages == null)
                {
                    errorMessages = new List<string>();
                    if (!ErrorTypes.HasValue)
                    {
                        errorMessages.Add("NullError");
                    }
                    else
                    {

                        foreach (ErrorTypes r in Enum.GetValues(typeof(ErrorTypes)))
                        {
                            if (ErrorTypes.Value.ToString().Trim() == "NoErrors" && r.ToString() == "NoErrors" ||
                                ErrorTypes.Value.HasFlag(r) && r.ToString().Trim() != "NoErrors")
                            {
                                errorMessages.Add(r.ToString());
                            }
                        }
                    }
                }
                return errorMessages;
            }
            set
            {
                errorMessages = value;
            }
        }

        public int? ErrorId
        {
            get
            {
                return ErrorTypes.HasValue ? (int?)ErrorTypes : -1;
            }
        }

        public FillCalcBaseResult(string customError)
        {
            ErrorMessages.Clear();
            ErrorMessages.Add(customError);
        }

        public FillCalcBaseResult(ErrorTypes? errorTypes)
        {
            ErrorTypes = errorTypes;
        }
    }
}
