﻿using SPX.FillCalculation;
using SPX.Cooling.MarleyPerfectFitAPI.Interface;

namespace SPX.Cooling.MarleyPerfectFitAPI.Domain
{
    public class CreateCharacteristicCurveInput : ICreateCharacteristicCurveInput
    {
        public SelectionInputs Input { get; set; }
        public Account AccountInfo { get; set; }
    }
}
