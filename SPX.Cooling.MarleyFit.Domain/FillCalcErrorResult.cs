﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;

namespace SPX.Cooling.MarleyFitAPI.Domain
{
    public class FillCalcErrorResult : FillCalcBaseResult, IFillCalcErrorResult
    {
        public float OutputValue { get; set; }
       
        public FillCalcErrorResult(ErrorTypes? errorTypes, float outputValue) : base (errorTypes)
        {
            OutputValue = outputValue;
        }
        public FillCalcErrorResult(string customError) : base(customError) { }
    }
}
