﻿// ********************************************************************************************************************************************
// Copyright (c) 2014 Computer Technology Solutions, Inc.  ALL RIGHTS RESERVED
// http://www.askcts.com
// Author: CTS, Inc.
// Product: SPX.Cooling.MarleyFit.Domain
// Version: 1.0.1
// 
// ********************************************************************************************************************************************

using System;
using System.Linq;
using System.Security.Principal;
using System.Threading;

namespace SPX.Cooling.MarleyFit.Domain.Authentication
{
    /// <summary>
    /// Enumeration of application roles that are valid for authenticated users.
    /// </summary>
    public enum ApplicationRoles
    {
        /// <summary>
        /// Users are administrator
        /// </summary>
        Administrator = 1,

        /// <summary>
        /// Users are normal user.
        /// </summary>
        User
    }

    /// <summary>
    /// Base on <see cref="System.Security.Principal.IPrincipal"/> to hold authenticated user information and used in place
    /// default <c>Principal</c> object.
    /// </summary>
    [Serializable]
    public class Principal : MarshalByRefObject, IPrincipal
    {

        /// <summary>
        /// Returns currently authenticated user.
        /// </summary>
        public static Principal Current
        {
            get { return Thread.CurrentPrincipal as Principal; }
        }

        /// <summary>
        /// Creates an unauthorized <see cref="SPX.Cooling.MarleyFit.Domain.Authentication.Principal"/>.
        /// </summary>
        /// <returns>Returns <see cref="SPX.Cooling.MarleyFit.Domain.Authentication.Principal"/> object.</returns>
        public static Principal GetUnauthorized()
        {
            return new Principal(SPX.Cooling.MarleyFit.Domain.Authentication.Identity.GetUnauthorized(), new ApplicationRoles[0]);
        }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Principal() { }

        /// <summary>
        /// Constructor to setup identity and roles.
        /// </summary>
        /// <param name="identity">User that is logged into the application.</param>
        /// <param name="roles">Roles of theused that is logged in.</param>
        public Principal(Identity identity, ApplicationRoles[] roles)
        {
            this.Identity = identity;
            this.Roles = roles;
        }

        /// <summary>
        /// Collection of role for currently authenticated user.
        /// </summary>
        public ApplicationRoles[] Roles { get; set; }

        /// <summary>
        /// Identity of the currently authenticated user.
        /// </summary>
        public IIdentity Identity { get; set; }

        /// <summary>
        /// IPrincipal implementation. Not used for this application.
        /// <see cref="IsInRole(ApplicationRoles)"/>
        /// </summary>
        /// <param name="role">Role to check.</param>
        /// <returns>true - if matching role is found in Roles collection.</returns>
        public bool IsInRole(string role)
        {
            throw new NotImplementedException("Use IsInRole(ApplicationRoles) method.");
        }

        /// <summary>
        /// Checks a role for the Principal.
        /// </summary>
        /// <param name="role">Role to check.</param>
        /// <returns>true - if matching role is found in Roles collection.</returns>
        public bool IsInRole(ApplicationRoles role)
        {
            return this.Roles.Any(x => (int)x == (int)role);
        }

        /// <summary>
        /// Supports access control for the roles. Admin has full access.
        /// </summary>
        /// <param name="role"></param>
        /// <returns>true - if the matching role(s) are found.</returns>
        public bool IsAuthorized(string role)
        {
            // admins are authorized to do anything
            return this.IsInRole(ApplicationRoles.Administrator) || this.IsInRole((ApplicationRoles)Enum.Parse(typeof(ApplicationRoles), role));
        }
    }
}
