﻿// ********************************************************************************************************************************************
// Copyright (c) 2014 Computer Technology Solutions, Inc.  ALL RIGHTS RESERVED
// http://www.askcts.com
// Author: CTS, Inc.
// Product: SPX.Cooling.MarleyFit.Domain
// Version: 1.0.1
// 
// ********************************************************************************************************************************************

using System;
using System.Security.Principal;
using System.Threading;

namespace SPX.Cooling.MarleyFit.Domain.Authentication
{
    ///<summary>
    /// Implementation of IIdentity for storing custom information.
    /// </summary>
    [Serializable]    
    public class Identity : MarshalByRefObject, IIdentity
    {
        /// <summary>
        /// Specifies NTLM authentication.
        /// </summary>
        public const string Windows = "Windows";

        /// <summary>
        /// Specifies forms authentication.
        /// </summary>
        public const string Forms = "Forms";

        /// <summary>
        /// Currently logged in user.
        /// </summary>
        public static Identity Current
        {
            get { return Thread.CurrentPrincipal.Identity as Identity; }
        }

        /// <summary>
        /// Initialize an Identity object for unauthorized user.
        /// </summary>
        /// <returns>Unauthorized Identity <see cref="SPX.Cooling.MarleyFit.Domain.Authentication.Identity"/> instance.</returns>
        public static Identity GetUnauthorized()
        {
            return new Identity() { IsAuthenticated = false };
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Identity()
        {
            this.AuthenticationType = Forms;
            this.IsAuthenticated = true;
        }

        /// <summary>
        /// AuthenticationType can be Forms or Windows
        /// </summary>
        public string AuthenticationType { get; set; }

        /// <summary>
        /// Contains bool value to determine if the use is authenticated.
        /// </summary>
        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// Authenticated user's name.
        /// </summary>
        public string Name { get; set; } // NTID

        /// <summary>
        /// Authenticated user's username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Authenticated user's Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Authenticated user's display name.
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// Authenticated user's email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Authenticated user's display name.
        /// </summary>
        public override string ToString()
        {
            return this.DisplayName;
        }

        /// <summary>
        /// Authenticated user's username with domain.
        /// </summary>
        /// <remarks>
        /// If the username has domain 'CTS\jdoe', then the user name 'CTS' is stripped out from the username.
        /// </remarks>
        public static string GetNameWithoutDomain(string username)
        {
            return username.Substring(username.IndexOf('\\') + 1);
        }
    }
}
