﻿// ********************************************************************************************************************************************
// Copyright (c) 2014 Computer Technology Solutions, Inc.  ALL RIGHTS RESERVED
// http://www.askcts.com
// Author: CTS, Inc.
// Product: SPX.Cooling.MarleyFit.Domain
// Version: 1.0.1
// 
// ********************************************************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPX.Cooling.MarleyFit.Domain.Authentication
{
   /// <summary>
   /// Currently not used. Future with DI implementation
   /// </summary>
   public interface IAuthService
   {
        /// <summary>
        /// Retrieves user from physical data store or cache.
        /// </summary>
        /// <param name="username">Used to retrieved data for a specific user.</param>
        /// <returns>Returns <see cref="SPX.Cooling.MarleyFit.Domain.Authentication.Principal"/> that consist authentication and user information.</returns>
        Principal GetUser(string username);
   }
}
