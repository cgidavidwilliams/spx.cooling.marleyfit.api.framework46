﻿using SPX.FillCalculation;

namespace SPX.Cooling.MarleyPerfectFitAPI.Domain
{
    public class SolveConditionInput
    {
        public SelectionInputs Input { get; set; }
        public Account AccountInfo { get; set; }
    }
}
