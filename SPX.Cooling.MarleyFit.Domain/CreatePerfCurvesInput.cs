﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;

namespace SPX.Cooling.MarleyFitAPI.Domain
{
    public class CreatePerfCurvesInput : BasicInput, ICreatePerfCurvesInput
    {
        public CurveInputs CurveLimits {get; set;}
    }
}
