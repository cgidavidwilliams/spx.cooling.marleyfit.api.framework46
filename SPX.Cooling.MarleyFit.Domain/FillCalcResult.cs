﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;
using System;
using System.IO;

namespace SPX.Cooling.MarleyFitAPI.Domain
{
    public class FillCalcResult : FillCalcBaseResult, IFillCalcResult
    {
        public string PDFStreamBase64String { get; set; }

        public FillCalcResult (ErrorTypes errorTypes, ref Stream stream) : base (errorTypes)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                byte[] bytes = memoryStream.ToArray();
                PDFStreamBase64String =  Convert.ToBase64String(bytes);
            }
        }

        public FillCalcResult(string customError) : base(customError)
        {
            PDFStreamBase64String = "";
        }
    }
}
