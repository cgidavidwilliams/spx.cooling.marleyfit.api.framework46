﻿using SPX.Cooling.MarleyFitAPI.Domain;
using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.FillCalculation;
//using SpxWsToolkit.Communication;
using System;
using System.Collections.Generic;
using System.IO;

namespace SPX.Cooling.MarleyFitAPI.Service
{
    public class FillCalcRepository : IFillCalcRepository
    {
        public IFillCalcErrorResult SolveCondition(SelectionInputs inputs, Account accountInfo)//, Dictionary<string, string> connectionStrings)
        {
            Selection selection = new Selection();
            FillCalcErrorResult result = null;
            try
            {
                ///Todo: pass in a 3rd parameter the connectionstrings???
                ///Not sure if there is a better way to do this.
                selection.SolveCondition(ref inputs, accountInfo);
                float output = 0f;
                switch (inputs.SolveFor)
                {
                    case SolveForVariables.MotorOutput:
                        output = inputs.MotorOutput;
                        break;
                    case SolveForVariables.ColdWater:
                        output = inputs.ColdWater;
                        break;
                    case SolveForVariables.WaterFlow:
                        output = inputs.WaterFlow;
                        break;
                    case SolveForVariables.WetBulb:
                        output = inputs.WetBulb;
                        break;
                    default:
                        throw new System.Exception("Invalid SolveFor from input");
                }
                result = new FillCalcErrorResult(selection.OutputErrors, output);
            }
            catch (Exception ex)
            {
                result = new FillCalcErrorResult(ex.Message);
                //EMail.EmailMessage("HM:SPX.Cooling.MarleyFitAPI SolveCondition error", ex.ToString(), _cc: "craig.hickman@spx.com");
            }
            return (IFillCalcErrorResult)result;
        }

        public IFillCalcResult CreateReport(SelectionInputs inputs, Account accountInfo)
        {
            Selection selection = new Selection();
            FillCalcResult result = null;
            try
            {
                Stream stream = selection.CreateReport(ref inputs, accountInfo);
                result = new FillCalcResult(selection.OutputErrors, ref stream);
            }
            catch (Exception ex)
            {
                result = new FillCalcResult(ex.Message);
                //EMail.EmailMessage("HM:SPX.Cooling.MarleyFitAPI CreateReport error", ex.ToString(), _cc: "craig.hickman@spx.com");
            }
            return (IFillCalcResult)result;
        }

        public IFillCalcResult CreatePerfCurves(SelectionInputs inputs, Account accountInfo, CurveInputs curveLimits)
        {
            Selection selection = new Selection();
            FillCalcResult result = null;
            try
            {
                Stream stream = selection.CreatePerfCurves(ref inputs, accountInfo, curveLimits);
                result = new FillCalcResult(selection.OutputErrors, ref stream);
            }
            catch (Exception ex)
            {
                result = new FillCalcResult(ex.Message);
                //EMail.EmailMessage("HM:SPX.Cooling.MarleyFitAPI CreatePerfCurves error", ex.ToString(), _cc: "craig.hickman@spx.com");
            }
            return (IFillCalcResult)result;
        }

        public IFillCalcResult CreateCharacteristicCurve(SelectionInputs inputs, Account accountInfo)
        {
            Selection selection = new Selection();
            FillCalcResult result = null;
            try
            {
                Stream stream = selection.CreateCharacteristicCurve(inputs, accountInfo);
                result = new FillCalcResult(selection.OutputErrors, ref stream);
            }
            catch (Exception ex)
            {
                result = new FillCalcResult(ex.Message);
                //EMail.EmailMessage("HM:SPX.Cooling.MarleyFitAPI CreateCharacteristicCurve error", ex.ToString(), _cc: "craig.hickman@spx.com");
            }
            return (IFillCalcResult)result;
        }
    }
}