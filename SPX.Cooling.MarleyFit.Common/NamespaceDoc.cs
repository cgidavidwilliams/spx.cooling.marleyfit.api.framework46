﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SPX.Cooling.MarleyFit.Common
{
   /// <summary>
   /// The <c>SPX.Cooling.MarleyFit.Common</c> contains implementations that are common to all layers. 
   /// </summary>
   internal class NamespaceDoc { }
}

namespace SPX.Cooling.MarleyFit.Common.Exceptions
{
   /// <summary>
   /// The <c>SPX.Cooling.MarleyFit.Common.Exceptions</c> contains exception classes that can originate from any layer.
   /// </summary>
   internal class NamespaceDoc { }
}

namespace SPX.Cooling.MarleyFit.Common.Helpers
{
   /// <summary>
   /// The <c>SPX.Cooling.MarleyFit.Common.Helpers</c> implement common helper functionalities for the application. The encapsulation of common implementation to Helpers 
   /// namespace increases the reuse and separation of concern. 
   /// </summary>
   internal class NamespaceDoc { }
}

namespace SPX.Cooling.MarleyFit.Common.GridEx
{
   /// <summary>
   /// The <c>SPX.Cooling.MarleyFit.Common.GridEx</c> implements paging and sorting for lists and grids. The presentation works PaginateList to perform server side 
   /// paging and sorting. The PaginageList also offers different ways to customize the results.
   /// </summary>
   internal class NamespaceDoc { }
}
