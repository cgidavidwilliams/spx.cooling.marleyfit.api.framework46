﻿using SPX.FillCalculation;
using System.Collections.Generic;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public interface IFillCalcRepository
    {
        IFillCalcErrorResult SolveCondition(SelectionInputs inputs, Account accountInfo);//, Dictionary<string,string> connectionStrings);
        IFillCalcResult CreateReport(SelectionInputs inputs, Account accountInfo);
        IFillCalcResult CreatePerfCurves(SelectionInputs inputs, Account accountInfo, CurveInputs curveLimits);
        IFillCalcResult CreateCharacteristicCurve(SelectionInputs inputs, Account accountInfo);
    }
}
