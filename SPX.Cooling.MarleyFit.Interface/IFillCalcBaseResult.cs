﻿using SPX.FillCalculation;
using System;
using System.Collections.Generic;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public interface IFillCalcBaseResult
    {
        List<string> ErrorMessages { get; set; }
        int? ErrorId { get;  }
        //void AddCustomError(string errorMsg);
    }
}
