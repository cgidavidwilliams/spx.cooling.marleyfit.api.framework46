﻿using SPX.FillCalculation;
using System;
using System.IO;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public interface IFillCalcResult : IFillCalcBaseResult
    {
        string PDFStreamBase64String { get; set; }
    }
}
