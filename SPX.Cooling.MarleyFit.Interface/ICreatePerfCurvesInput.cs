﻿using SPX.FillCalculation;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public interface ICreatePerfCurvesInput : IBasicInput
    {
        CurveInputs CurveLimits {get; set;}
    }
}
