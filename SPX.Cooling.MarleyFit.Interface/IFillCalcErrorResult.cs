﻿using SPX.FillCalculation;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public  interface IFillCalcErrorResult : IFillCalcBaseResult
    {
        float OutputValue { get; set; }
    }
}
