﻿using SPX.FillCalculation;
using System;

namespace SPX.Cooling.MarleyFitAPI.Interface
{
    public interface IBasicInput
    {
        SelectionInputs Input { get; set; }
        Account AccountInfo { get; set; }
    }
}
