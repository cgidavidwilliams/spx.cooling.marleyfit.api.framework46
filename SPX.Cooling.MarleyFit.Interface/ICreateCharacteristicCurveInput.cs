﻿using SPX.FillCalculation;

namespace SPX.Cooling.MarleyPerfectFitAPI.Interface
{
    public interface ICreateCharacteristicCurveInput
    {
        SelectionInputs Input { get; set; }
        Account AccountInfo { get; set; }
    }
}
