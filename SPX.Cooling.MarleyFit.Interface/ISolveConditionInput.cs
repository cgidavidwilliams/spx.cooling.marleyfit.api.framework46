﻿using SPX.FillCalculation;

namespace SPX.Cooling.MarleyPerfectFitAPI.Interface
{
    public interface ISolveConditionInput
    {
        SelectionInputs Input { get; set; }
        Account AccountInfo { get; set; }
    }
}
