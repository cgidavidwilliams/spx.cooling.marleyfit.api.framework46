﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.Common
{
    public sealed class Encryption
    {
        private TripleDESCryptoServiceProvider _DES = null;

        public string Decrypt(string msg)
        {
            // Create a Decryptor based on the key and transform the data
            ICryptoTransform DESDecrypt = _DES.CreateDecryptor();
            byte[] buffer = Convert.FromBase64String(msg);
            return ASCIIEncoding.Unicode.GetString(DESDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
        }

        public string Encrypt(string msg)
        {
            // Create an Encryptor based on the key and transform the data
            ICryptoTransform DESEncrypt = _DES.CreateEncryptor();
            byte[] buffer = ASCIIEncoding.Unicode.GetBytes(msg);
            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));
        }

        public Encryption(string key = "tb76jq-2MHX8Bcq~#c)P.CZ(RVe#j9EqjX'2tG2`=J})!g_fr`m+A3fz?a;RjQ}-")
        {
            // Transform the key into a useable HASH
            _DES = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
            _DES.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            _DES.Mode = CipherMode.ECB;
        }
    }
}
