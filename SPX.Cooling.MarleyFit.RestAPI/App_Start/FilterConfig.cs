﻿using System.Web;
using System.Web.Mvc;

namespace SPX.Cooling.MarleyFit.RestAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
