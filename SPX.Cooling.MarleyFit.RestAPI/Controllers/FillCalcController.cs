﻿
using SPX.Cooling.MarleyFitAPI.Domain;
using SPX.Cooling.MarleyFitAPI.Interface;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.v1.Controllers
{

    public class FillCalcController : BaseApiController // ApiController
    {
        private readonly IFillCalcRepository _fillCalcRepository;

        public FillCalcController(IFillCalcRepository fillCalcRepository)
        {
            _fillCalcRepository = fillCalcRepository;
        }

        [HttpPost]
        [Route("SolveCondition", Name = nameof(SolveCondition))]
        //[Authorize(AuthenticationSchemes = MarleyFitWebTokenAuthOptions.DefaultScheme)]
        public IFillCalcErrorResult SolveCondition(IBasicInput basicInput)
        {
            if (basicInput == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            IFillCalcErrorResult result = _fillCalcRepository.SolveCondition(basicInput.Input, basicInput.AccountInfo);//, connectionStringDictionary);
            return result;
        }

        [HttpPost]
        [Route("CreateReport", Name = nameof(CreateReport))]
        //[Authorize(AuthenticationSchemes = MarleyFitWebTokenAuthOptions.DefaultScheme)]
        public IFillCalcResult CreateReport([FromBody] IBasicInput basicInput)
        {
            if (basicInput == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            IFillCalcResult result = _fillCalcRepository.CreateReport(basicInput.Input, basicInput.AccountInfo);
            return result;
        }

        [HttpPost]
        [Route("CreatePerfCurves", Name = nameof(CreatePerfCurves))]
        //[Authorize(AuthenticationSchemes = MarleyFitWebTokenAuthOptions.DefaultScheme)]
        public IFillCalcResult CreatePerfCurves([FromBody] ICreatePerfCurvesInput createPerfCurvesInput)
        {
            if (createPerfCurvesInput == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            IFillCalcResult result = _fillCalcRepository.CreatePerfCurves(createPerfCurvesInput.Input, createPerfCurvesInput.AccountInfo, createPerfCurvesInput.CurveLimits);
            return result;
        }

        [HttpPost]
        [Route("CreateCharacteristicCurve", Name = nameof(CreateCharacteristicCurve))]
        //[Authorize(AuthenticationSchemes = MarleyFitWebTokenAuthOptions.DefaultScheme)]
        public IFillCalcResult CreateCharacteristicCurve([FromBody] IBasicInput basicInput)
        {
            if (basicInput == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ""));
            }

            IFillCalcResult result = _fillCalcRepository.CreateCharacteristicCurve(basicInput.Input, basicInput.AccountInfo);
            return result;
        }
    }
}
