﻿using SPX.Cooling.MarleyFitAPI.Interface;
using SPX.Cooling.MarleyFitAPI.Service;
using System.Configuration;
using System.Web.Http;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.v1.Controllers
{
    public class BaseApiController : ApiController
    {
        private readonly IFillCalcRepository _fillCalcRepository;

        public BaseApiController()
        {
            _fillCalcRepository = new FillCalcRepository();
        }
    }
}