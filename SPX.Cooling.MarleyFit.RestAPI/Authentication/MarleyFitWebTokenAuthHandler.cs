﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using SPX.Cooling.MarleyFitAPI.WebAPI.Common;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.Authentication
{
    public class MarleyFitWebTokenAuthHandler : AuthenticationHandler<MarleyFitWebTokenAuthOptions>
    {
        private readonly Configuration.BasicAuthenticationTokens _basicAuthenticationTokens;
        public MarleyFitWebTokenAuthHandler(IOptionsMonitor<MarleyFitWebTokenAuthOptions> options, 
            IOptions<Configuration.BasicAuthenticationTokens> basicAuthenticationTokensAccessor, 
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) 
            : base(options, logger, encoder, clock)
        {
            _basicAuthenticationTokens = basicAuthenticationTokensAccessor.Value;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            // Get Authorization header value
            if (!Request.Headers.TryGetValue(HeaderNames.Authorization, out var authorization))
            {
                return Task.FromResult(AuthenticateResult.Fail("Cannot read authorization header."));
            }

            // The auth key from Authorization header check against the configured ones
            string[] keys = authorization.ToString().Split(',');

            Encryption enc = new Encryption();
            if (_basicAuthenticationTokens.MarleyFitWebToken.RestAPIKey != enc.Encrypt(keys[0]) ||
                _basicAuthenticationTokens.MarleyFitWebToken.ApplicationName != keys[1] ||
                _basicAuthenticationTokens.MarleyFitWebToken.ApplicationKey != enc.Encrypt(keys[2])
                )
            {
                return Task.FromResult(AuthenticateResult.Fail("Invalid auth key."));
            }
        
            // Create authenticated user
            var identities = new List<ClaimsIdentity> {new ClaimsIdentity("custom auth type")};
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identities), Options.Scheme);

            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
}