﻿using System;
using Microsoft.AspNetCore.Authentication;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.Authentication
{
    public static class AuthenticationBuilderExtensions
    {
        // Custom authentication extension method
        public static AuthenticationBuilder AddCustomAuth(this AuthenticationBuilder builder, Action<CustomAuthOptions> configureOptions)
        {
            // Add custom authentication scheme with custom options and custom handler
            return builder.AddScheme<CustomAuthOptions, CustomAuthHandler>(CustomAuthOptions.DefaultScheme, configureOptions);
        }

        public static AuthenticationBuilder AddMarleyFitWebTokenAuth(this AuthenticationBuilder builder, Action<MarleyFitWebTokenAuthOptions> configureOptions)
        {
            // Add custom authentication scheme with custom options and custom handler
            return builder.AddScheme<MarleyFitWebTokenAuthOptions, MarleyFitWebTokenAuthHandler>(MarleyFitWebTokenAuthOptions.DefaultScheme, configureOptions);
        }
    }
}
