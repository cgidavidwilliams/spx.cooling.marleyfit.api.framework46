﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace SPX.Cooling.MarleyFitAPI.WebAPI.Authentication
{

    public class MarleyFitWebTokenAuthOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "MarleyFitWeb auth";
        public string Scheme => DefaultScheme;
    }
}