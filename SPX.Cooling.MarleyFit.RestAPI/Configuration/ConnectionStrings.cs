﻿namespace SPX.Cooling.MarleyFitAPI.WebAPI.Configuration
{
    public sealed class ConnectionStrings
    {
        public string SpxSystemConnection { get; set; }
        public string SPXComponentsConnection { get; set; }
    }
}
