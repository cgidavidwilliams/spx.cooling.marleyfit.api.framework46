﻿namespace SPX.Cooling.MarleyFitAPI.WebAPI.Configuration
{
    public sealed class BasicAuthenticationTokens
    {
        public MarleyFitWebToken MarleyFitWebToken { get; set; }
    }

    public sealed class MarleyFitWebToken
    {
        public string RestAPIKey { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationKey { get; set; }
    }
}
